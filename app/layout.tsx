import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { AuthProvider } from "@/context/AuthContext";
import { ThemeProvider } from "@/context/ThemeContext";
import takshishila from "../public/takshshila.jpeg"

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "TechShila Minds",
  description: "A comprehensive it training platform",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <AuthProvider>
      <ThemeProvider>
        <html lang="en">
          <body style={{
            //  backgroundImage: `url("${takshishila.src}")`
          }} className={inter.className}>{children}</body>
        </html>
      </ThemeProvider>
    </AuthProvider>
  );
}
