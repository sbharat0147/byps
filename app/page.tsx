'use client'
import Image from "next/image";
import AppAppBar from "@/components/AppAppBar";
import * as React from 'react';
import { PaletteMode } from '@mui/material';
import Features from "@/components/Features";
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import getLPTheme from "./getLPTheme";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import AutoAwesomeRoundedIcon from '@mui/icons-material/AutoAwesomeRounded';
import Hero from "@/components/Hero";
import LogoCollection from "@/components/LogoCollection";
import Footer from "@/components/Footer";
import FAQ from "@/components/FAQ";
import Testimonials from "@/components/Testimonials";
import Divider from '@mui/material/Divider';
import Pricing from "@/components/Pricing";
import Highlights from "@/components/Highlights";
import Box from '@mui/material/Box';
import { AuthProvider } from "@/context/AuthContext";
import { useThemeContext } from "@/context/ThemeContext";

const defaultTheme = createTheme({});

interface ToggleCustomThemeProps {
  showCustomTheme: Boolean;
  toggleCustomTheme: () => void;
}

function ToggleCustomTheme({
  showCustomTheme,
  toggleCustomTheme,
}: ToggleCustomThemeProps) {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100dvw',
        position: 'fixed',
        bottom: 24,
      }}
    >
      <ToggleButtonGroup
        color="primary"
        exclusive
        value={showCustomTheme}
        onChange={toggleCustomTheme}
        aria-label="Platform"
        sx={{
          backgroundColor: 'background.default',
          '& .Mui-selected': {
            pointerEvents: 'none',
          },
        }}
      >
        {/* <ToggleButton value>
          <AutoAwesomeRoundedIcon sx={{ fontSize: '20px', mr: 1 }} />
          Custom theme
        </ToggleButton> */}
        {/* <ToggleButton value={false}>Material Design 2</ToggleButton> */}
      </ToggleButtonGroup>
    </Box>
  );
}

export default function Home() {
  // const [mode, setMode] = React.useState<PaletteMode>('dark');
  const [showCustomTheme, setShowCustomTheme] = React.useState(true);
  const { mode, toggleMode } = useThemeContext();
  const LPtheme = createTheme(getLPTheme(mode));

  const toggleColorMode = () => {
    toggleMode();
    // setMode((prev) => (prev === 'dark' ? 'light' : 'dark'));
  };

  const toggleCustomTheme = () => {
    setShowCustomTheme((prev) => !prev);
  };

  return (
    <ThemeProvider theme={showCustomTheme ? LPtheme : defaultTheme}>
    <Box>
      <CssBaseline />
      <AppAppBar mode={mode} toggleColorMode={toggleColorMode} showMenus={true}/>
      <Hero />
      <Box sx={{ bgcolor: 'background.primary' }}>
        <LogoCollection />
        <Features />
        <Divider />
        <Highlights />
        <Divider />
        <Testimonials />
        <Divider />
        <Pricing />
        <Divider />
        <FAQ />
        <Divider />
        <Footer />
      </Box>
      <ToggleCustomTheme
        showCustomTheme={showCustomTheme}
        toggleCustomTheme={toggleCustomTheme}
      />
      </Box>
    </ThemeProvider>
  );
}
