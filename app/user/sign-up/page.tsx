'use client'
import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import MenuItem from '@mui/material/MenuItem';
import { jwtDecode } from 'jwt-decode';
import Cookies from 'js-cookie';
import { useRouter } from 'next/navigation';
import { useAuth } from '@/context/AuthContext';
import { useThemeContext } from '@/context/ThemeContext';
import { useTheme } from '@mui/system';
import getLPTheme from '@/app/getLPTheme';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Copyright(props: any) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://stag.1click.tech/">
        BYPS
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const defaultTheme = createTheme();

export default function SignUp() {
  const [isSubmitting, setIsSubmitting] = React.useState(false);
  const [gender, setGender] = React.useState('');
  const {login} = useAuth();
  const router = useRouter();
  const { mode } = useThemeContext();
  const theme = useTheme();
  const LPtheme = createTheme(getLPTheme('dark'));

  const handleGenderChange = (event:any) => {
    setGender(event.target.value);
  };

  const handleSignUp = async (data : FormData) => {
    try {
      const baseUrl = process.env.NEXT_PUBLIC_AUTH_BASE_URL;

      if (!baseUrl) {
        console.error("Base URL is not defined");
        return;
      }

      const response = await fetch(`${baseUrl}/users/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: data.get('name'),
          surname: data.get('surname'),
          username: data.get('username'),
          gender: data.get('gender'),
          email: data.get('email'),
          mobileNumber: data.get('mobileNumber'),
          password: data.get('password'),
          optionalDepartmentRoles:[`${process.env.NEXT_PUBLIC_OPTIONALDEPARTMENTROLES}`],
          optionalProjectRoles:[`${process.env.NEXT_PUBLIC_OPTIONALPROJECTROLE}`],
          projectId: `${process.env.NEXT_PUBLIC_EXAM_APP_PROJECT_ID}`,
        }),
      });

      if (response.ok) {
        toast.success('Sign-up successful. Redirecting to sign-in.', { autoClose: 1500 });
        setTimeout(() => {
          router.push('/user/sign-in');
        }, 2000);
      } else {
        const errorMessage = await response.text();
        toast.error(`Sign-up failed: ${errorMessage}`, { autoClose: 5000 });
      }
    } catch (error) {
      toast.error(`Sign-up failed: ${error}`, {autoClose: 2000});
    } finally {
      setIsSubmitting(false);
    }
  };
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    setIsSubmitting(true);
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    handleSignUp(data);
  };

  return (
    <ThemeProvider theme={LPtheme}>
      <ToastContainer />
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="name"
                  required
                  size="small"
                  variant="outlined"
                  fullWidth
                  id="name"
                  arial-label="First Name"
                  placeholder="First Name"
                  autoFocus
                  hiddenLabel
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  size="small"
                  variant="outlined"
                  id="surname"
                  arial-label="Last Name"
                  name="surname"
                  placeholder="Last Name"
                //   autoComplete="family-name"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                //   autoComplete="given-name"
                  name="username"
                  required
                  fullWidth
                  size="small"
                  variant="outlined"
                  id="username"
                  arial-label="Username"
                  placeholder="Username"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  size="small"
                  variant="outlined"
                  fullWidth
                  id="gender"
                  arial-label="Gender"
                  name="gender"
                  select
                  value={gender}
                  placeholder="Gender"
                  onChange={handleGenderChange}
                >
                  <MenuItem value="" disabled>
                    Select Gender
                  </MenuItem>
                  <MenuItem value="MALE">Male</MenuItem>
                  <MenuItem value="FEMALE">Female</MenuItem>
                </TextField>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  size="small"
                  variant="outlined"
                  id="email"
                  arial-label="Email Address"
                  name="email"
                  placeholder="Email Address"
                //   autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  size="small"
                  variant="outlined"
                  id="mobileNumber"
                  arial-label="Mobile Number"
                  name="mobileNumber"
                  placeholder="Mobile Number"
                //   autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  size="small"
                  variant="outlined"
                  name="password"
                  arial-label="Password"
                  type="password"
                  id="password"
                  placeholder="Password"
                //   autoComplete="new-password"
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox value="allowExtraEmails" color="primary" />}
                  label="I want to receive inspiration, marketing promotions and updates via email."
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              disabled={isSubmitting}
            >
              {isSubmitting ? 'Getting you registered...' : 'Sign Up'}
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link href="#" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>
  );
}
