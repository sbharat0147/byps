'use client'
import Image from "next/image";
import AppAppBar from "@/components/AppAppBar";
import * as React from 'react';
import { PaletteMode } from '@mui/material';
import Features from "@/components/Features";
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import getLPTheme from "../getLPTheme";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import AutoAwesomeRoundedIcon from '@mui/icons-material/AutoAwesomeRounded';
import Hero from "@/components/Hero";
import LogoCollection from "@/components/LogoCollection";
import Footer from "@/components/Footer";
import FAQ from "@/components/FAQ";
import Testimonials from "@/components/Testimonials";
import Divider from '@mui/material/Divider';
import Pricing from "@/components/Pricing";
import Highlights from "@/components/Highlights";
import Box from '@mui/material/Box';
import { AuthProvider } from "@/context/AuthContext";
import { useThemeContext } from "@/context/ThemeContext";
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import bgBack from '../../public/home-background4.jpeg'
import { alpha } from '@mui/material';
import Grid from '@mui/material/Grid';

const defaultTheme = createTheme({});

interface ToggleCustomThemeProps {
  showCustomTheme: Boolean;
  toggleCustomTheme: () => void;
}

function ToggleCustomTheme({
  showCustomTheme,
  toggleCustomTheme,
}: ToggleCustomThemeProps) {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100dvw',
        position: 'fixed',
        bottom: 24,
      }}
    >
      <ToggleButtonGroup
        color="primary"
        exclusive
        value={showCustomTheme}
        onChange={toggleCustomTheme}
        aria-label="Platform"
        sx={{
          backgroundColor: 'background.default',
          '& .Mui-selected': {
            pointerEvents: 'none',
          },
        }}
      >
      </ToggleButtonGroup>
    </Box>
  );
}

export default function Home() {
  const [showCustomTheme, setShowCustomTheme] = React.useState(true);
  const { mode, toggleMode } = useThemeContext();
  const LPtheme = createTheme(getLPTheme(mode));

  const toggleColorMode = () => {
    toggleMode();
  };

  const toggleCustomTheme = () => {
    setShowCustomTheme((prev) => !prev);
  };

  return (
    <ThemeProvider theme={showCustomTheme ? LPtheme : defaultTheme}>
    <Box>
      <CssBaseline />
      <AppAppBar mode={mode} toggleColorMode={toggleColorMode} showMenus={false}/>
      <Box
        id="hero"
        sx={(theme) => ({
            width: '100%',
            backgroundSize: '100% 20%',
            backgroundRepeat: 'no-repeat',
        })}
        >
        <Container
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                pt: { xs: 14, sm: 20 },
                pb: { xs: 8, sm: 12 },
            }}
        >
            <Stack spacing={2} useFlexGap sx={{ width: { xs: '100%', sm: '70%' } }}>
            <Typography
                    component="h4"
                    variant="h4"
                    sx={{
                    display: 'flex',
                    flexDirection: { xs: 'column', md: 'row' },
                    alignSelf: 'center',
                    textAlign: 'center',
                }}
            >
                Who are we ?&nbsp;
            </Typography>
            <Typography variant="body1" textAlign="center" color="text.secondary">
                A sister concern of 1 Click Capital (a fintech startup providing finances to various businesses including some notable large-scale businesses) and 1 Click Policy (an insurance aggregator startup) under 1 Click Global Services, is a leading IT services provider offering SaaS-based solutions, AI and ML-powered automation, and 24×7 virtual DevOps team without the need for huge costs. We empower businesses to succeed in today’s digital world with cutting-edge technology and reliable service. Join us on our journey to build the future of technology, one click at a time.  <br />
                <br />
                Elevate Your Business with Our SaaS Solutions: Scalable, Flexible, and Efficient. Harness the power of 1Click Tech's innovative SaaS offerings to optimize resources, foster collaboration, and drive innovation. Stay ahead in today's dynamic market with our scalable and flexible solutions, designed to empower your business for sustained growth and success.
            </Typography>
            </Stack>
            <Box
            id="image"
            sx={(theme) => ({
                mt: { xs: 8, sm: 10 },
                alignSelf: 'center',
                height: { xs: 200, sm: 700 },
                width: '100%',
                backgroundImage:
                theme.palette.mode === 'light'
                    ? `url(${bgBack.src})`
                    : `url(${bgBack.src})`,
                backgroundSize: 'cover',
                borderRadius: '10px',
                outline: '1px solid',
                outlineColor:
                theme.palette.mode === 'light'
                    ? alpha('#BFCCD9', 0.5)
                    : alpha('#9CCCFC', 0.1),
                boxShadow:
                theme.palette.mode === 'light'
                    ? `0 0 12px 8px ${alpha('#9CCCFC', 0.2)}`
                    : `0 0 24px 12px ${alpha('#033363', 0.2)}`,
            })}
            />
        </Container>
        </Box>
        <Divider />
        <Box
        id="hero"
        sx={(theme) => ({
            width: '100%',
            backgroundSize: '100% 20%',
            backgroundRepeat: 'no-repeat',
        })}
        >
        <Container
            sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                pt: { xs: 5, sm: 5 },
                pb: { xs: 8, sm: 12 },
            }}
        >
            <Grid container spacing={2} sx={{ width: { xs: '100%', sm: '100%' } }} alignItems="center">
            <Grid item xs={12} md={6}>
            <Box
            id="image"
            sx={(theme) => ({
                mt: { xs: 8, sm: 10 },
                alignSelf: 'center',
                height: { xs: 200, sm: 700 },
                width: '100%',
                backgroundImage:
                theme.palette.mode === 'light'
                    ? `url(${bgBack.src})`
                    : `url(${bgBack.src})`,
                backgroundSize: 'cover',
                borderRadius: '10px',
                outline: '1px solid',
                outlineColor:
                theme.palette.mode === 'light'
                    ? alpha('#BFCCD9', 0.5)
                    : alpha('#9CCCFC', 0.1),
                boxShadow:
                theme.palette.mode === 'light'
                    ? `0 0 12px 8px ${alpha('#9CCCFC', 0.2)}`
                    : `0 0 24px 12px ${alpha('#033363', 0.2)}`,
            })}
            />
            </Grid>
        <Grid item xs={12} md={6}>
            <Stack spacing={2} useFlexGap sx={{ width: { xs: '100%', sm: '100%' } }}>
            <Typography
                    component="h4"
                    variant="h4"
                    sx={{
                    display: 'flex',
                    flexDirection: { xs: 'column', md: 'row' },
                    alignSelf: 'center',
                    textAlign: 'center',
                }}
            >
                Note from Our CEO&nbsp;
            </Typography>
            <Typography variant="body1" textAlign="center" color="text.secondary" sx={{px:4}}>
            We at 1 Click Tech believe in the power of education to transform lives, and this platform embodies our commitment to providing innovative tools for student success. With adaptive assessments, real-time feedback, and a user-friendly interface, we're redefining the learning experience. It's about accessibility, adaptability, and creating opportunities for every student. As we launch this platform, your feedback becomes our compass for improvement. Together, let's shape a future where education knows no bounds.
                <br/>
                <br />
                - Jay Singh
            </Typography>
            </Stack>
            </Grid>
            
            </Grid>
        </Container>
        </Box>
      <Box sx={{ bgcolor: 'background.primary' }}>
        <LogoCollection />
        <Divider />
        <Footer />
      </Box>
      <ToggleCustomTheme
        showCustomTheme={showCustomTheme}
        toggleCustomTheme={toggleCustomTheme}
      />
      </Box>
    </ThemeProvider>
  );
}
