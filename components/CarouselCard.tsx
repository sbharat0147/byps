import React from 'react';
import Slider from "react-slick";
import {Box, Card} from '@mui/material';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const sliderSettings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true
};



// @ts-ignore
export const CarouselCard = ({ items, selectedItemIndex,})=> {
    console.log(items, selectedItemIndex)
    return (
        <Card
            variant="outlined"
            sx={{
                height: '100%',
                width: '100%',
                display: { xs: 'none', sm: 'flex' },
                pointerEvents: 'none',
                margin: 0,
            }}
        >
            <Box
                sx={{
                    m: 'auto',
                    width: '95%',
                    height: '70%',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <Slider {...sliderSettings}>
                    {items.map((item: { imageLight: any; imageDark: any; }, index: React.Key | null | undefined) => (
                        <Box
                            key={index}
                            sx={{
                                width: '100%',
                                height: '100%',
                                backgroundSize: 'cover',
                                backgroundImage: (theme) =>
                                    theme.palette.mode === 'light'
                                        ? item.imageLight
                                        : item.imageDark
                            }}
                        />
                    ))}
                </Slider>
            </Box>
        </Card>
    );
};
