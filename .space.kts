job("otes-ui - Production: Build and push Docker") {
    startOn {
        gitPush {
            branchFilter {
                +"refs/heads/main"
            }
        }
    }
    host("Build artifacts and a Docker image") {
        env["HUB_USER"] = "0fcac7bb-54d9-4024-a7c1-2a669e9e060f"
        env["HUB_TOKEN"] = Secrets("spacejob_pat")

        shellScript {
            // login to Docker Hub
            content = """
                docker login 1clicktech.registry.jetbrains.space --username ${'$'}HUB_USER --password "${'$'}HUB_TOKEN"
            """
        }

        dockerBuildPush {
            labels["vendor"] = "1clicktech"
            file = "Dockerfile.production"
            val spaceRepo = "1clicktech.registry.jetbrains.space/p/otes-ai/otes-ai/otes-ui"
            // image tags for 'docker push'
            tags {
                +"$spaceRepo:1.2.${"$"}JB_SPACE_EXECUTION_NUMBER"
                +"$spaceRepo:latest"
            }
        }
    }

    container(displayName = "Kubectl", image = "gtsopour/awscli-kubectl") {
        env["AWS_ACCESS_KEY_ID"] = Secrets("git-user-aws-access-key")
        env["AWS_SECRET_ACCESS_KEY"] = Secrets("git-user-aws-secret-key")
        // ping service 5 times
        shellScript {
            content = """
                aws eks update-kubeconfig --region ap-south-1 --name 1click-prod-eks-cluster
                kubectl rollout restart deployment -n uat otes-ui
            """
        }
    }

    container("openjdk:11") {
        kotlinScript { api ->
            api.space().projects.automation.deployments.start(
                    project = api.projectIdentifier(),
                    targetIdentifier = TargetIdentifier.Key("otes-ui"),
                    version = "1.2." + System.getenv("JB_SPACE_EXECUTION_NUMBER"),
                    // automatically update deployment status based on a status of a job
                    syncWithAutomationJob = true
            )
        }
    }
}

job("otes-ui - Development/UAT: Build and push Docker") {
    startOn {
        gitPush {
            branchFilter {
                +"refs/heads/development"
            }
        }
    }
    host("Build artifacts and a Docker image") {
        env["HUB_USER"] = "0fcac7bb-54d9-4024-a7c1-2a669e9e060f"
        env["HUB_TOKEN"] = Secrets("spacejob_pat")

        shellScript {
            // login to Docker Hub
            content = """
                docker login 1clicktech.registry.jetbrains.space --username ${'$'}HUB_USER --password "${'$'}HUB_TOKEN"
            """
        }

        dockerBuildPush {
            labels["vendor"] = "1clicktech"
            file = "Dockerfile.development"
            val spaceRepo = "1clicktech.registry.jetbrains.space/p/otes-ai/otes-ai/otes-ui"
            // image tags for 'docker push'
            tags {
                +"$spaceRepo:dev-1.2.${"$"}JB_SPACE_EXECUTION_NUMBER"
                +"$spaceRepo:develop"
            }
        }
    }

    container(displayName = "Kubectl", image = "gtsopour/awscli-kubectl") {
        env["AWS_ACCESS_KEY_ID"] = Secrets("git-user-aws-access-key")
        env["AWS_SECRET_ACCESS_KEY"] = Secrets("git-user-aws-secret-key")
        // ping service 5 times
        shellScript {
            content = """
                aws eks update-kubeconfig --region ap-south-1 --name 1click-prod-eks-cluster
                kubectl rollout restart deployment -n uat otes-ui
            """
        }
    }

    container("openjdk:11") {
        kotlinScript { api ->
            api.space().projects.automation.deployments.start(
                    project = api.projectIdentifier(),
                    targetIdentifier = TargetIdentifier.Key("otes-ui"),
                    version = "dev-1.2." + System.getenv("JB_SPACE_EXECUTION_NUMBER"),
                    // automatically update deployment status based on a status of a job
                    syncWithAutomationJob = true
            )
        }
    }
}