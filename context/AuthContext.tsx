'use client'
import React, { createContext, useContext, useState, ReactNode } from "react";
import PropTypes from "prop-types";
import Cookies from "js-cookie";

interface AuthProviderProps {
  children: ReactNode;
}

interface AuthContextValue {
  token: string | null;
  username: string | null;
  login: (newToken: string, newUsername: string) => void;
  logout: () => void;
}

const AuthContext = createContext<AuthContextValue | undefined>(undefined);

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [token, setToken] = useState<string | null>(null);
  const [username, setUsername] = useState<string | null>(null);

  const login = (newToken: string, newUsername: string) => {
    setToken(newToken);
    setUsername(newUsername);
  };

  const logout = () => {
    setToken(null);
    setUsername(null);
    Cookies.remove("accessToken", {
      secure: true,
    });
    Cookies.remove("refreshToken", {
      secure: true,
    });
    Cookies.remove("username", {
      secure: true,
    });
    window.location.reload();
  };

  return (
    <AuthContext.Provider value={{ token, username, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export const useAuth = (): AuthContextValue => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth must be used within an AuthProvider");
  }
  return context;
};

export default AuthContext;
