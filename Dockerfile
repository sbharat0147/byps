# build environment
FROM node:18-alpine as build
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
RUN npm ci --silent
COPY . ./
COPY .env.development .env.production
RUN npm run build
RUN npm install pm2 -g
EXPOSE 80
CMD sh -c 'pm2-runtime npm --name otes-ui -- run start -- -p 80'